from flask import Flask
from flask import request
from flask import jsonify
from flask import render_template
from flask import Response
from flask import make_response
import json

app = Flask(__name__)

messages = []
last_len = 0


@app.route("/client", methods=['GET'])
def client():
    return render_template("client.html")


@app.route("/", defaults={'path': ''}, methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
@app.route("/<path:path>", methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
def main(path):
    def pack(data=''):
        package = {
            'body': data,
            'method': request.method,
            'baseUrl': request.base_url,
            'headers': request.headers.to_list(),
            'params': request.args,
        }
        messages.append(package)

    def json_message(code=200):
        print(request.data)
        data = request.get_json(True)
        pack(data)
        return make_response(jsonify(data), code)

    if request.method == 'GET':
        pack()
        return '1'
    elif request.method == 'POST':
        return json_message(201)
    else:
        return json_message(200)

@app.route("/stream")
def stream():
    def event_stream():
        global messages
        global last_len
        # while True:
        if len(messages) > last_len:
            for package in messages[last_len:]:
                yield "event: request\ndata: {}\n\n".format(json.dumps(package))
            last_len = len(messages)
    
    return Response(event_stream(), mimetype="text/event-stream")
