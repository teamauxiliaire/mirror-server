# README #

##### A minimal mirroring server written in Python (Flask) #####

The server accepts requests, and mirrors (echoes) them back. Ideal to mock third party services.

### Features ###

* Support for the following methods:

    * GET
    * POST
    * PUT
    * PATCH
    * DELETE
    
* Supports arbitrary paths (but GET /client)
* Captures query parameters
* Captures JSON body
* Captures all headers
* Admin Dashboard to conveniently follow the requests: [/client](http://127.0.0.1:5000/client)

### Requirements ###

```commandline
pip install Flask
```

See Flask docs for details: [Installation](http://flask.pocoo.org/docs/0.12/installation/#installation)

### How to run ###

* Linux:
    
    FLASK_APP=app.py flask run
    
    or:
    
    export FLASK_APP=app.py
    
    flask run

* Windows:

    set FLASK_APP=app.py
    
    flask run

### Usage ###

* Send a request to [http://127.0.0.1:5000/](http://127.0.0.1:5000/) with any content you'd like to get back as response
* Check the standard output or the client [Dashboard](http://127.0.0.1:5000/client) to follow the incoming requests.