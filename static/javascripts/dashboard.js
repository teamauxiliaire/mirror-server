var eventSource = new EventSource("/stream");
var DashboardModel = function() {
	var self = this;

	self.lines = ko.observableArray();
	self.addLine = function(line) {
		if (line != '') {
			self.lines.push(line);
		}
	}.bind(self);

	// Routing
	self.dashboardPage = ko.observable('dashboard');
	self.aboutPage = ko.observable();

	self.goToDashboard = function() { location.hash = '' };
	self.goToAbout = function() { location.hash = 'about' };

    Sammy(function() {
        this.get('#:page', function() {
            if(this.params.page == 'dashboard') {
                self.aboutPage(null);
                self.dashboardPage('dashboard');
            } else {
                self.dashboardPage(null);
                self.aboutPage('about');
            }
        });

        this.get('', function() { this.app.runRoute('get', '#dashboard') });
    }).run();
}
var model = new DashboardModel();

eventSource.addEventListener("request", function(e) {
	console.dir(e.data);
    model.addLine(JSON.parse(e.data));
});

ko.applyBindings(model);