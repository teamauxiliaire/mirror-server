var badge = function(method) {
    var level = '';
    switch(method) {
        case 'GET':
            level = 'success';
            break;
        case 'POST':
            level = 'warning';
            break;
        case 'DELETE':
            level = 'danger';
            break;
        case 'PUT':
            level = 'primary';
            break;
        default:
            level = 'secondary'
    }
    return `badge badge-${level}`;
};